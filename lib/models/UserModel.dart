class UserModel {
  bool authSuccess;
  String login;

  static UserModel failed() {
    var user = UserModel();
    user.authSuccess = false;
    return user;
  }
  static UserModel success(String login) {
    var user = UserModel();
    user.login = login;
    user.authSuccess = true;
    return user;
  }
}
