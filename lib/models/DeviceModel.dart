import 'package:device_repository/models/UserModel.dart';

class DeviceModel {
  String id;
  String serialNo;
  String type;
  String description;
  UserModel owner;
  DateTime createdAt;

  static DeviceModel make(
      String type, String description, String serialNo, UserModel owner) {
    var model = DeviceModel();
    model.type = type;
    model.description = description;
    model.serialNo = serialNo;
    model.owner = owner;
    model.createdAt = DateTime.now();
    model.id = model.createdAt.millisecondsSinceEpoch.toRadixString(16);
    return model;
  }

  static DeviceModel fromJson(Map data) {
    var blank = DeviceModel();
    blank.id = data['id'];
    blank.serialNo = data['serialNo'];
    blank.type = data['type'];
    blank.description = data['description'];
    blank.owner = UserModel.success(data['owner']);
    blank.createdAt = DateTime.fromMillisecondsSinceEpoch(data['createdAt']);
    return blank;
  }

  toJson() {
    return {
      'id': id,
      'serialNo': serialNo,
      'type': type,
      'description': description,
      'owner': owner.login,
      'createdAt': createdAt.millisecondsSinceEpoch
    };
  }
}
