import 'dart:convert';

import 'package:device_repository/models/DeviceModel.dart';
import 'package:device_repository/services/RootService.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeviceStore {
  String currentLogin;
  SharedPreferences sharedPreferences;
  ValueNotifier<List<DeviceModel>> total;
  init(sp) async {
    sharedPreferences = sp;
    total = ValueNotifier<List<DeviceModel>>(List());
  }

  add(DeviceModel device) {
    total.value.add(device);
    total.notifyListeners();
    save();
  }

  save() {
    String json = jsonEncode(total.value);
    sharedPreferences.setString('devices-${currentLogin}', json);
  }

  List<DeviceModel> get statList {
    if (total.value.length < 5) {
      return total.value;
    }
    List<DeviceModel> devices = total.value;
    devices.sort((a, b) {
      return a.createdAt.millisecondsSinceEpoch
          .compareTo(b.createdAt.millisecondsSinceEpoch);
    });
    return devices.getRange(0, 5).toList();
  }

  void restore(String login) {
    currentLogin = login;
    if (sharedPreferences.containsKey('devices-${login}')) {
      String json = sharedPreferences.getString('devices-${login}');
      List parsed = jsonDecode(json);
      if (parsed != null && parsed.length > 0) {
        List<DeviceModel> devices = List<DeviceModel>.generate(
            parsed.length, (i) => DeviceModel.fromJson(parsed[i]));
        List<DeviceModel> filteredDevices = devices.where((device) {
          return device.owner.login == RootService().authService.user.login;
        }).toList();
        total.value = filteredDevices;
      }
    }
  }
}
