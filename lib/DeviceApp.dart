import 'package:device_repository/screens/AddDeviceScreen.dart';
import 'package:device_repository/screens/AuthScreen.dart';
import 'package:device_repository/screens/DeviceListScreen.dart';
import 'package:device_repository/screens/LoaderScreen.dart';
import 'package:device_repository/screens/StatScreen.dart';
import 'package:flutter/material.dart';

class DeviceApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/add/': (ctx) => AddDeviceScreen(),
        '/stat/': (ctx) => StatScreen(),
        '/auth/': (ctx) => AuthScreen(),
        '/list/': (ctx) => DeviceListScreen(),
      },
      home: LoaderScreen(),
    );
  }
}
