import 'dart:io';

import 'package:flutter/material.dart';

class DeviceListDrawer extends StatelessWidget {
  _navigateToAddition(context) =>
      Navigator.pushNamed(context, '/add/');

  _navigateToList(context) => Navigator.pushReplacementNamed(context, '/list/');

  _navigateToStat(context) => Navigator.pushReplacementNamed(context, '/stat/');

  _exit() {
    exit(0);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          DrawerHeader(
            child: Container(color: Colors.lightBlue),
            padding: EdgeInsets.zero,
          ),
          ListTile(
            onTap: () => _navigateToAddition(context),
            title: Text('Добавить устройство'),
          ),
          ListTile(
            onTap: () => _navigateToList(context),
            title: Text('Список'),
          ),
          ListTile(
            onTap: () => _navigateToStat(context),
            title: Text('Статистика'),
          ),
          ListTile(
            onTap: _exit,
            title: Text('Выйти'),
          ),
        ],
      ),
    );
  }
}
