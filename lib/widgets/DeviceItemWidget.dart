import 'package:device_repository/models/DeviceModel.dart';
import 'package:device_repository/screens/DeviceDetailScreen.dart';
import 'package:flutter/material.dart';


class DeviceItem extends StatelessWidget {
  final DeviceModel deviceModel;

  goToDetail(context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (ctx) => DeviceDetailScreen(deviceModel: deviceModel)));
  }

  const DeviceItem({Key key, @required this.deviceModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () => goToDetail(context),
      title: Text(deviceModel.serialNo),
      subtitle: Text(deviceModel.createdAt.toIso8601String()),
    );
  }
}
