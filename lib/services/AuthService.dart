import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:device_repository/models/UserModel.dart';
import 'package:device_repository/stores/DeviceStore.dart';

class AuthService {
  DeviceStore deviceStore;
  UserModel user;
  final List _salts = [
    "f6fdffe48c908deb0f4c3bd36c032e72",
    "5cc32e366c87c4cb49e4309b75f57d64"
  ];

  init(ds) {
    deviceStore = ds;
  }

  Future<UserModel> auth(String login, String password) async {
    // симулируем задержку сети
    await Future.delayed(Duration(seconds: 1));
    String salt = md5.convert(utf8.encode(login + password)).toString();
    print(salt == _salts[0]);
    this.user =
        _salts.contains(salt) ? UserModel.success(login) : UserModel.failed();

    if (user.authSuccess) {
      deviceStore.restore(user.login);
    }
    return user;
  }
}
