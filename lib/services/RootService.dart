import 'package:device_repository/services/AuthService.dart';
import 'package:device_repository/stores/DeviceStore.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RootService {
  SharedPreferences sp;
  static final RootService _singleton = RootService._internal();
  factory RootService() => _singleton;
  RootService._internal();

  AuthService authService;

  DeviceStore deviceStore;
  init(AuthService authService, DeviceStore deviceStore) async {
    sp = await SharedPreferences.getInstance();
    this.authService = authService;
    this.deviceStore = deviceStore;
    await this.deviceStore.init(sp);
    await authService.init(deviceStore);
  }
}
