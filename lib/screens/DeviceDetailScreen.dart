import 'package:device_repository/models/DeviceModel.dart';
import 'package:flutter/material.dart';

class DeviceDetailScreen extends StatelessWidget {
  final DeviceModel deviceModel;

  const DeviceDetailScreen({Key key, this.deviceModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(deviceModel.id.toString())),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(deviceModel.type),
              subtitle: Text('Тип'),
            ),
            ListTile(
              title: Text(deviceModel.serialNo),
              subtitle: Text('Серийный номер'),
            ),
            ListTile(
              title: Text(deviceModel.description),
              subtitle: Text('Описание'),
            ),
            ListTile(
              title: Text(deviceModel.id),
              subtitle: Text('ID'),
            ),
            ListTile(
              title: Text(deviceModel.owner.login),
              subtitle: Text('Владелец'),
            ),
            ListTile(
              title: Text(deviceModel.createdAt.toIso8601String()),
              subtitle: Text('Время создания'),
            )
          ],
        ),
      ),
    );
  }
}
