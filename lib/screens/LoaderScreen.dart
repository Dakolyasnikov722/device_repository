import 'package:device_repository/services/AuthService.dart';
import 'package:device_repository/services/RootService.dart';
import 'package:device_repository/stores/DeviceStore.dart';
import 'package:flutter/material.dart';

class LoaderScreen extends StatefulWidget {
  @override
  _LoaderScreenState createState() => _LoaderScreenState();
}

class _LoaderScreenState extends State<LoaderScreen> {
  @override
  void initState() {
    /// В данном случае можно было бы использовать Provider для размещения 
    /// Всех сервисов и сторов. Правило трех (если меньше 3 сущностей - делай проще)
    /// намекает на то, что нужно делать проще и использовать сервис локатор с вынесением 
    /// создания зависимостей
    super.initState();
    RootService().init(
      AuthService(),
      DeviceStore()
    ).then(_navigateToAuth);
  }

  _navigateToAuth(empty) {
    Navigator.of(context).pushReplacementNamed('/auth/');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: CircularProgressIndicator()),
    );
  }
}
