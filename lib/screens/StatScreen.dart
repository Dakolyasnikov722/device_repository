import 'package:device_repository/services/RootService.dart';
import 'package:device_repository/widgets/DeviceItemWidget.dart';
import 'package:device_repository/widgets/DeviceListDrawer.dart';
import 'package:flutter/material.dart';

class StatScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List devices = RootService().deviceStore.statList;
    return Scaffold(
      drawer: DeviceListDrawer(),
      appBar: AppBar(title: Text('Статистика')),
      body: ListView(
        children: List.generate(devices.length, (i) {
          return DeviceItem(deviceModel: devices[i]);
        }),
      ),
    );
  }
}
