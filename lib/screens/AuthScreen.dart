import 'package:device_repository/models/UserModel.dart';
import 'package:device_repository/services/RootService.dart';
import 'package:flutter/material.dart';

class AuthScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> sk = GlobalKey<ScaffoldState>();
  final TextEditingController login = TextEditingController();
  final TextEditingController pass = TextEditingController();

  _tryAuth(context) async {
    sk.currentState.showSnackBar(SnackBar(
      content: Text('Попытка авторизации'),
    ));
    UserModel user =
        await RootService().authService.auth(login.text, pass.text);
    _afterAttempt(user, context);
  }

  _afterAttempt(UserModel user, context) {
    if (user.authSuccess) {
      Navigator.pushNamedAndRemoveUntil(context, '/list/', (r) => false);
    } else {
      sk.currentState.hideCurrentSnackBar();
      sk.currentState.showSnackBar(SnackBar(
        backgroundColor: Colors.red[400],
        content: Text(
          'Не верный пароль или логин',
          style: TextStyle(color: Colors.white),
        ),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Авторизация')),
      key: sk,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: TextField(
                decoration: InputDecoration(hintText: "Логин"),
                controller: login,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 12.0),
              child: TextField(
                decoration: InputDecoration(hintText: "Пароль"),
                controller: pass,
              ),
            ),
            RaisedButton(
              onPressed: () => _tryAuth(context),
              child: Text('Авторизоваться'),
            )
          ],
        ),
      ),
    );
  }
}
