import 'package:device_repository/models/DeviceModel.dart';
import 'package:device_repository/models/UserModel.dart';
import 'package:device_repository/services/RootService.dart';
import 'package:device_repository/widgets/DeviceItemWidget.dart';
import 'package:device_repository/widgets/DeviceListDrawer.dart';
import 'package:flutter/material.dart';

class DeviceListScreen extends StatelessWidget {
  _navigateToAdd(context) {
    Navigator.pushNamed(context, '/add/');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: DeviceListDrawer(),
      appBar: AppBar(title: Text('Список устройств')),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _navigateToAdd(context),
        child: Icon(Icons.add),
      ),
      body: ValueListenableBuilder(
        valueListenable: RootService().deviceStore.total,
        builder: (ctx, deviceList, ch) => ListView(
          children: List.generate(deviceList.length, (i) {
            return DeviceItem(deviceModel: deviceList[i]);
          }),
        ),
      ),
    );
  }
}
