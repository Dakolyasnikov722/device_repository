import 'package:device_repository/models/DeviceModel.dart';
import 'package:device_repository/services/RootService.dart';
import 'package:flutter/material.dart';

class AddDeviceScreen extends StatelessWidget {
  final TextEditingController type = TextEditingController();
  final TextEditingController description = TextEditingController();
  final TextEditingController serialNo = TextEditingController();

  _submit(context) {
    var device = DeviceModel.make(type.text, description.text, serialNo.text,
        RootService().authService.user);
    print(device);
    RootService().deviceStore.add(device);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Добавить устройство')),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            // crossAxisAlignment: CrossAxisAlignment,
            children: <Widget>[
              TextField(
                controller: type,
                decoration: InputDecoration(
                  hintText: 'Тип',
                ),
              ),
              TextField(
                controller: serialNo,
                decoration: InputDecoration(
                  hintText: 'Сериный номер',
                ),
              ),
              TextField(
                controller: description,
                minLines: 4,
                maxLines: 6,
                decoration: InputDecoration(
                  hintText: 'Описание',
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  onPressed: () => _submit(context),
                  child: Text(
                    'Добавить',
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Theme.of(context).accentColor,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
